import java.util.Scanner;
public class Application{
	public static void main (String [] args){
		Scanner reader = new Scanner(System.in);
		
		// name, id, isCurrentStudent, grade, rScore are a type already created in Student.java
		Student disciple = new Student("noNameYet", 0); //1
		// creating an instance for the class "Student" to use its type for storing mutilple data type together
		System.out.println(disciple.getName());
		//disciple.setName("Alex");
		System.out.println(disciple.getName());
		//disciple.setId(2236697);
		//disciple.setStatus(false);
		//disciple.setGrade(60);
		//disciple.setRScore(24);
		Student disciple2 = new Student("noNameYet", 0);  //2
		System.out.println(disciple2.getName());
	//	disciple2.setName("Elle");
		System.out.println(disciple2.getName());

	//	disciple2.setId(2875643);
	//	disciple2.setStatus(true);
	//	disciple2.setGrade(95);
	//	disciple2.setRScore(35);
		Student disciple3 = new Student("noNameYet", 0); //3
		System.out.println(disciple3.getName());
	//	disciple3.setName("Him");
		System.out.println(disciple3.getName());
		
	//	disciple3.setId(23641254);
	//	disciple3.setStatus(true);
	//	disciple3.setGrade(80);
	//	disciple3.setRScore(28);
		
		// new student
		Student disciple4 = new Student( "New student" , 25);
		
		disciple4.setId(2236697);
		disciple4.setStatus(false);
		disciple4.setGrade(60);
		disciple4.setRScore(24);
		
		System.out.println(disciple4.getName());
		System.out.println(disciple4.getId());
		System.out.println(disciple4.getStatus());
		System.out.println(disciple4.getGrade());
		System.out.println(disciple4.getRScore());
		System.out.println(disciple4.getAmountLearnt());
		//disciple2.passExam();
		Student[] section4 = new Student[3];
		
		section4[0] =  disciple;
		section4[1] = 	disciple2;
		section4[2] = disciple3;
		//System.out.println(section4[0].id);
	    //System.out.println(section4[2].rScore);
		// using loops to fill the array
		/*
		for (int index = 0 ; index <section4.length; index++){
			System.out.println(section4[index].name);
			System.out.println(section4[index].rScore);
			System.out.println(section4[index].id);
			System.out.println(section4[index].isCurrentStudent);
			System.out.println(section4[index].grade);
			// also call the methods on each of them
			section4[index].passExam();
			
			System.out.println(" next student ");
		
		section4[index].canGoMCGill();
		}
		*/
		
		
		//System.out.println(" enter the amount of hours studied");
		
		//int amountStudied = Integer.parseInt(reader.nextLine()); // input of hours studeid
		
		//section4[2].study(amountStudied); // adds the Scanner input to the hours studied 
		//section4[2].study(amountStudied); // twice
		
		//System.out.println(disciple.getName()); // gets the disciple name from getName method of Student class
		
		
		//System.out.println(section4[2].amountLearnt);
	
	/*
		
		for(int index = 0 ; index < section4.length; index++){
		System.out.println(section4[index].amountLearnt);
		}
*/		
}
}